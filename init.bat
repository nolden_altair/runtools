::@echo off
set HERE=%~dp0

:: you can use envy_defaults.bat or envy_defaults.exe (or whatever can be expanded) 
:: either in this folder, or somewhere on the path. 
:: Alternatively you put the stuff into your machines 
:: environment variables
where envy_defaults > nul
if not ERRORLEVEL 1 (
	::echo Loading envy_defaults.bat
  call envy_defaults.bat
)
set HW_ROOTDIR=%ALTAIRINST%

echo ;%PATH%; | find /C /I ";%HERE%;" > nul || set PATH=%HERE%;%PATH%
if not defined DEFAULT_BUILD exit /B 1
echo Setting environment for build:  [41m %DEFAULT_BUILD% [0m
call setenv-%DEFAULT_BUILD%

:: end of 'envy_defaults'

set UNITY_DIR=unity

if exist "%HW_ROOTDIR%\common\python" (
	set UNITY_DIR=hwx
)

if exist "%HW_ROOTDIR%\..\common\python" (
	set UNITY_DIR=hwx
)

set HW_THIRDPARTY_WIN=%HW_THIRDPARTY:/=\%


  :: it's an install (at least has python)
  :: it's a build (ot sure, that works anyway
if %UNITY_DIR% == "hwx" (
	set PYDIR=%HW_ROOTDIR%\common\python\python3.8\%OS_BITS%
	set PY_EXE="%PYDIR%\python.exe"
) else (
	set PYDIR=%HW_THIRDPARTY_WIN%\python\python3.8.10\%OS_BITS%
	if %OS_BITS% == win64d (set suffix=_d) else (set suffix=)
		
	set PY_EXE="%PYDIR%\python%suffix%.exe"
	
)
echo ;%PATH%; | find /C /I ";%PYDIR%\python3.8.10\%OS_BITS%\Scripts;"  > nul || set PATH=%PATH%;%PYDIR%\Scripts


:: for mypy (and maybe linting in general), it might be
:: good to set these paths to PYTHONPATH
:: set PYTHONPATH=%HW_ROOTDIR%\mv\scripts\python;%HW_ROOTDIR%\hw\python
::set MYPYPATH=%PYDIR%\DLLs;%PYDIR%\lib;%PYDIR%\;%PYDIR%\lib\site-packages;%PYDIR%\lib\site-packages\win32;%PYDIR%\lib\site-packages\win32\lib;%PYDIR%\lib\site-packages\Pythonwin;%PYDIR%\lib\site-packages\IPython\extensions
