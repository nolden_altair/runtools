@echo off
call init

set HW_ENABLE_UNITY_POST=1
set HW_HWPY_GUI=1

:: folowing needed, when skipping runhwx
set HW_UNITY_ROOTDIR=%HW_ROOTDIR%\%UNITY_DIR%
set ALTAIR_HOME=%HW_ROOTDIR%
echo on
"%HW_ROOTDIR%\%UNITY_DIR%\bin\%OS_BITS%\hwx.exe" -c HyperWorksDesktop -plugin HyperworksPost -profile HyperworksPost -clientconfig hwmbdmodel.dat %2
