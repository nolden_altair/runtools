@echo off
call init


echo "%suffix%"
echo "%PY_EXE%"


setlocal

if exist %HW_ROOTDIR%\hwdesktop (
	set PYTHONPATH=%PYTHONPATH%;%HW_ROOTDIR%/hwdesktop/mv/scripts/python
) else (
	set PYTHONPATH=%PYTHONPATH%;%HW_ROOTDIR%/mv/scripts/python
)

%PY_EXE%  %*

endlocal
exit /B 0

