
:: setting up python path to be similar to HW
set PYTHONPATH=C:\p4\troy\MautoFb21_ud2022_0_0_6\hw\python;C:\p4\troy\MautoFb21_ud2022_0_0_3\mv\scripts\python;C:\p4\troy\MautoFb21_ud2022_0_0_3\unity\plugins\hwd\profiles\HyperworksPost

::set LINTRC=c:/work/repositories/imports.pylintrc
::set LINTRC=c:/work/repositories/simple.pylintrc
set LINTRC=c:/work/repositories/.pylintrc
python -m pylint --rcfile %LINTRC% ^
  hw.mview.remote hw.mview.repository hw.mview.browserhandler   ^
  hw.mview.repository.plugins.builtin                           ^
  model                                                         ^
  clients.MBD.widgets.indexbrowser                              ^ 
  clients.MBD.widgets.constrol                                  ^ 
  clients.MBD.widgets.menu                                      ^ 
  clients.MBD.utilities.failing                                 ^ 
  clients.MBD.utilities.profiledelegate                         ^ 
  clients.MBD.utilities.remote


::  model.data.repository.plugins.dummy                           ^
::  model.data.repository.plugins.filesystem                      ^
::  model.data.repository.plugins.pip                             ^

:: python -m pylint --rcfile c:/work/repositories/imports.pylintrc model
:: python -m pylint --rcfile c:/work/repositories/imports.pylintrc clients.MBD

::python -m pylint --rcfile c:/work/repositories/imports.pylintrc

:: cleanup
set PYTHONPATH=