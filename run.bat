@echo on
set HERE=%~dp0
call "%HERE%init"
if not defined DEFAULT_RUN (
  echo No default project to run is set, run a project explicitly by calling:
	echo   run-unity
	echo or
	echo   run-classic
  exit /B 1
)
echo run-%DEFAULT_RUN% %*
run-%DEFAULT_RUN% %*

:: D:\p4\troy\MViewFb22_Bfsa32022_3_0_15\unity\bin\win64\runhwx.exe -client HyperWorksDesktop -plugin HyperworksLauncher -profile HyperworksLauncher -l en -sf plugins/hwd/resources/splashHyperWorks.png