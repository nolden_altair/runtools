# runtools

## Getting started

Clone this into a project folder and customize it to point to your sandbox


## Description

It should help running HyperWorks, especially MotionView with certain environment variables and in different modes (release/debug)




## Installation

just clone this enywhere and edit envy_defaults.bat

## Usage

use 'run' to run whatever build is configured in the project directory

## Support

nolden@altair.com

## Roadmap

removing stuff from dual process and non-unity invokations

removing stuff from old python installs

## Contributing

Contributions are not planned, but let me know if you have any

## Authors and acknowledgment

Me, myself and I 

## License

public domain, do what you like

## Project status

in flow
